package com.stylee.lifestylee;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class Showactivity extends AppCompatActivity {

    TextView tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10, tv11, tv12,
            tv13, tv14, tv15, tv16, tv17, tv18, tv19, tv20, tv21, tv22, tv23,
            tv24, tv25, tv26, tv27, tv28, tv29, tv30, tv31, tv32, tv33, tv34,
            tv35;
    ImageView im;

    int pos = -1;

    public Integer images[] = { R.drawable.imi, R.drawable.imi,
            R.drawable.imi, R.drawable.imi, R.drawable.imi,
            R.drawable.imi, R.drawable.imi, R.drawable.imi,
            R.drawable.imi, R.drawable.imi, R.drawable.imi,
            R.drawable.imi, };
    static final String AD_UNIT_ID = "ca-app-pub-8444650851472007/9771210576";

    AdView adView;
    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //For Full Screen view----------------------
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_item);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        setTitle("Beauty Care");


        tv1 = (TextView) findViewById(R.id.textss1);
        tv2 = (TextView) findViewById(R.id.textss2);
        tv3 = (TextView) findViewById(R.id.textss3);
        tv4 = (TextView) findViewById(R.id.textss4);
        tv5 = (TextView) findViewById(R.id.textss5);
        tv6 = (TextView) findViewById(R.id.textss6);
        tv7 = (TextView) findViewById(R.id.textss7);
        tv8 = (TextView) findViewById(R.id.textss8);
        tv9 = (TextView) findViewById(R.id.textss9);
        tv10 = (TextView) findViewById(R.id.textss10);
        tv11 = (TextView) findViewById(R.id.textss11);
        tv12 = (TextView) findViewById(R.id.textss12);
        tv13 = (TextView) findViewById(R.id.textss13);



        getIntent().getExtras().getString("head");
        tv1.setText("" + getIntent().getStringExtra("hd"));

        getIntent().getExtras().getString("str1");
        tv2.setText("" + getIntent().getStringExtra("st1"));

        getIntent().getExtras().getString("str2");
        tv3.setText("" + getIntent().getStringExtra("st2"));

        getIntent().getExtras().getString("str3");
        tv4.setText("" + getIntent().getStringExtra("st3"));

        getIntent().getExtras().getString("str4");
        tv5.setText("" + getIntent().getStringExtra("st4"));

        getIntent().getExtras().getString("str5");
        tv6.setText("" + getIntent().getStringExtra("st5"));

        getIntent().getExtras().getString("str6");
        tv7.setText("" + getIntent().getStringExtra("st6"));

        getIntent().getExtras().getString("str7");
        tv8.setText("" + getIntent().getStringExtra("st7"));

        getIntent().getExtras().getString("str8");
        tv9.setText("" + getIntent().getStringExtra("st8"));

        getIntent().getExtras().getString("str9");
        tv10.setText("" + getIntent().getStringExtra("st9"));

        getIntent().getExtras().getString("str10");
        tv11.setText("" + getIntent().getStringExtra("st10"));

        getIntent().getExtras().getString("str11");
        tv12.setText("" + getIntent().getStringExtra("st11"));

        getIntent().getExtras().getString("str12");
        tv13.setText("" + getIntent().getStringExtra("st12"));


        // Admob code
        adView = new AdView(this);
        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId(AD_UNIT_ID);
        linearLayout = (LinearLayout) findViewById(R.id.add_linera);
        linearLayout.addView(adView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice(AD_UNIT_ID)
                .build();
        adView.loadAd(adRequest);




        im = (ImageView) findViewById(R.id.showimage_view);

        pos = getIntent().getExtras().getInt("pos");

        im.setBackgroundResource(images[pos]);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        super.finish();
        Intent intent = new Intent(Showactivity.this, ListActivity.class);
        overridePendingTransition(R.anim.right_in, R.anim.right_out);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}