package com.stylee.lifestylee;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class Splash  extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        // getActionBar().hide();
        setContentView(R.layout.activity_splash);

        Thread thread = new Thread() {

            @Override
            public void run() {
                try {
                    sleep(3000);
                    startActivity(new Intent(getApplicationContext(),
                            MainActivity.class));
                    overridePendingTransition(R.anim.abc_slide_in_bottom,
                            R.anim.abc_slide_in_top);
                    finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }

        };

        thread.start();

    }

}